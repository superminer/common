package common

import (
	"os"
	"fmt"
)

var debugOn = false

func ReadDebugFlag() {
	args := os.Args
	for _, val := range args {
		if val == "-debug" || val == "-debug=true" {
			fmt.Println("Starting as debug mode")
			debugOn = true
		}
	}
}

func DebugMode() bool {
	return debugOn
}
