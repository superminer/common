package common

import (
	"github.com/hashicorp/consul/api"
	"math/rand"
	"fmt"
	"encoding/json"
	"errors"
)

type ConsulInterface interface {
	QueryService(serviceName string) (string, error)
	RegisterServices(serviceConfig []byte) error
}

type Consul struct {
	client *api.Client
}

var agent *Consul
var mockAgent ConsulInterface

/**
提供一个mockAgent，方便本机调试用
如果设置了MockAgent，则返回mockAgent，否则返回正式的agent
 */
func ConsulAgent() ConsulInterface {
	if mockAgent != nil {
		return mockAgent
	}
	if agent == nil {
		agent = &Consul{}
		agent.Init()
	}
	return agent
}

func SetMockAgentForDebug(ma ConsulInterface) {
	mockAgent = ma
}

func (c *Consul) Init()  {
	client, err := api.NewClient(api.DefaultConfig())
	if err != nil {
		panic(err)
	}
	c.client = client
}


/**
query service by service name
return endpoint, eg: 127.0.0.1:1080
 */
func (c *Consul) QueryService(serviceName string) (string, error) {
	catalog := c.client.Catalog()
	services, _, err := catalog.Service(serviceName, "", nil)
	if err != nil {
		return "", err
	}
	if len(services) == 0 {
		return "", errors.New("no service queried")
	}
	// random choose from existing services
	service := services[rand.Intn(len(services))]
	address := service.ServiceAddress
	if address == "" {
		address = service.Address
	}
	return fmt.Sprintf("%s:%d", address, service.ServicePort), nil
}

/**
通过json配置注册service
 */
func (c *Consul) RegisterServices(serviceConfig []byte) error {
	agent := c.client.Agent()
	var serviceRegisters []*api.AgentServiceRegistration
	err := json.Unmarshal(serviceConfig, &serviceRegisters)
	if err != nil {
		return err
	}
	for _, val := range serviceRegisters {
		err := agent.ServiceRegister(val)
		if err != nil {
			return err
		}
	}
	return nil
}