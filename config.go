package common

import (
	"github.com/micro/go-config"
	"github.com/micro/go-config/source/file"
	"path/filepath"
)

var Config config.Config

func InitConfig() {
	InitConfigByName("config.json")
}

func InitConfigByName(configName string) {
	Config = config.NewConfig()
	Config.Load(file.NewSource(
		file.WithPath(filepath.Join(GetExecutableDir(), configName))))
}
